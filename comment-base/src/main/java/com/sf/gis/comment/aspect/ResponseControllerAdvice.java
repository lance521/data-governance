/**
 * Copyright (C), 2015-2022
 * FileName: ResponseControllerAdvice
 * Author:   caiguofang
 * Date:     2022/3/12 14:21
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.aspect;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sf.gis.comment.exception.APIException;
import com.sf.gis.comment.result.Result;
import lombok.SneakyThrows;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 〈〉
 * @ClassName ResponseControllerAdvice
 * @author Administrator
 * @create 2022/3/12 14:21
 * @since 1.0.0
 */
//@RestControllerAdvice(basePackages = {"com.suruomo.unified.controller"}) // 注意哦，这里要加上需要扫描的包
public class ResponseControllerAdvice implements ResponseBodyAdvice<Object> {
    @SneakyThrows
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> aClass) {
        // 如果接口返回的类型本身就是ResultVO那就没有必要进行额外的操作，返回false
//        System.out.println( Class.forName(returnType.getNestedGenericParameterType().getTypeName()).isAssignableFrom(Result.class) );
        return !(returnType.getNestedGenericParameterType().getTypeName().contains(Result.class.getTypeName())) ;
    }
    @Override
    public Object beforeBodyWrite(Object data, MethodParameter returnType, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // String类型不能直接包装，所以要进行些特别的处理
        if (returnType.getGenericParameterType().equals(String.class)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                // 将数据包装在Result里后，再转换为json字符串响应给前端
                return objectMapper.writeValueAsString(new Result<>(data));
            } catch (JsonProcessingException e) {
                throw new APIException();
            }
        }
        // 将原本的数据包装在ResultVO里
        return new Result<>(data);
    }
}