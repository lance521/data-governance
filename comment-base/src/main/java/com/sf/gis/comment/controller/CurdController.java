package com.sf.gis.comment.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sf.gis.comment.page.PageBase;
import com.sf.gis.comment.result.Result;
import com.sf.gis.comment.result.ResultCode;
import com.sf.gis.comment.result.ResultUtil;
import com.sf.gis.comment.service.BaseCurdService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class CurdController<Bs extends BaseCurdService, T extends PageBase<T>> {


    @Autowired
    protected Bs baseService;
    /**
     * 数据保存
     * @param t
     * @return
     */
    @PostMapping(path = "save")
    @ApiOperation(value = "新增", notes = "新增" )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="body" , name  ="save" )
    })
    public Result save(@RequestBody T t){
        baseService.save(t);
        return ResultUtil.okSuccess();
    }


    /**
     * ID进行软删除
     * @param t
     * @return
     */
    @RequestMapping(path = "delete", method = RequestMethod.POST)
    @ApiOperation(value = "删除", notes = "软删除, 前端需要isdeleted 修改成 1 " )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="body" , name  ="delete" )
    })
    public Result deleteById(@RequestBody T t){
        baseService.updateById(t);
        return ResultUtil.okSuccess();
    }

    /**
     * 更新操作
     * @param t
     * @return
     */
    @RequestMapping(path = "update", method = RequestMethod.POST)
    @ApiOperation(value = "更新", notes = "更新" )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="body" , name  ="update" )
    })
    public Result updateById(@RequestBody T t){
        baseService.updateById(t);
        return ResultUtil.okSuccess();
    }


    /**
     * 根据ID进行查询
     * @param id
     * @return
     */
    @RequestMapping(path = "queryByid", method = RequestMethod.POST)
    @ApiOperation(value = "根据ID查询", notes = "根据ID查询" )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="body" , name  ="queryByid" )
    })
    public Result queryById(@RequestParam(name = "id") String id){
        if(baseService instanceof   BaseCurdService) {
            return ResultUtil.okSuccess(baseService.queryByid(id));
        }
        return  ResultUtil.noFailed(ResultCode.FAILED.code(), ResultCode.FAILED.msg());
    }


    /**
     * 根据查询条件获取所有的数据List<T></>
     * @param t
     * @return
     */
    @RequestMapping(path = "queryList", method = RequestMethod.POST)
    @ApiOperation(value = "根据查询条件查询", notes = "根据查询条件查询" )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="query" , name  ="queryByEntity" )
    })
    public Result<List<T>> queryList(@RequestBody T t){
        return ResultUtil.okSuccess(baseService.queryList(t));

    }
    /**
     * 根据查询条件分页查询IPage<T></></>
     * @param t
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询" )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="query" , name  ="queryByEntity" )
    })
    @RequestMapping(path = "queryPage", method = RequestMethod.POST)
    public  Result<IPage<T>> queryPage(@RequestBody T t){
        return ResultUtil.okSuccess(baseService.queryPage(t));
    }
}
