/**
 * Copyright (C), 2015-2022
 * FileName: CurdServiceImpl
 * Author:   caiguofang
 * Date:     2022/3/13 14:53
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sf.gis.comment.page.PageBase;
import com.sf.gis.comment.service.BaseCurdService;

import java.util.List;

/**
 * 〈〉
 * @ClassName CurdServiceImpl
 * @author Administrator
 * @create 2022/3/13 14:53
 * @since 1.0.0
 */

public  class CurdServiceImpl<M extends BaseMapper<T>, T extends PageBase<T>>
        extends  ServiceImpl<M ,T>  implements BaseCurdService<T> {

    @Override
    public T queryByid(String id) {
        return super.baseMapper.selectById(id);
    }

    @Override
    public List<T> queryList(T t) {
        QueryWrapper<T> queryWrapper=new QueryWrapper<>();
        queryWrapper.setEntity(t);
        return super.baseMapper.selectList(queryWrapper);
    }

    @Override
    public IPage<T> queryPage(T t ) {
        //获取查询条件
        QueryWrapper<T> queryWrapper  = new QueryWrapper<>();
        queryWrapper.setEntity(t);


        Page page  = new Page(t.getPage(), t.getSize());
        return this.baseMapper.selectPage(page, queryWrapper);
    }

}