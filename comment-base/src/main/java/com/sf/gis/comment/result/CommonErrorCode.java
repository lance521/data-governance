/**
 * Copyright (C), 2015-2022
 * FileName: CommonErrorCode
 * Author:   caiguofang
 * Date:     2022/3/11 19:11
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.result;

import lombok.Data;

/**
 * 错误码信息
 * @ClassName CommonErrorCode
 * @author Administrator
 * @create 2022/3/11 19:11
 * @since 1.0.0
 */

@Data
public class CommonErrorCode {
    private String code;
    private String message;
}