/**
 * Copyright (C), 2015-2022
 * FileName: GlobalExceptionHandler
 * Author:   caiguofang
 * Date:     2022/3/12 14:14
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.exception;

import com.sf.gis.comment.result.Result;
import com.sf.gis.comment.result.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.jws.WebResult;
import java.util.ArrayList;
import java.util.List;

/**
 * 〈〉
 * @ClassName GlobalExceptionHandler
 * @author Administrator
 * @create 2022/3/12 14:14
 * @since 1.0.0
 */

@Slf4j
public class GlobalExceptionHandler {
    /**
     * 自定义异常APIException
     */
    @ExceptionHandler(APIException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<Object> APIExceptionHandler(APIException e) {
//        log.error("api异常");
        if (e.getCode()==null  ||  e.getCode().isEmpty()){
            return  new Result<>(ResultCode.FAILED,e.getMsg());
        }
        return  new Result<>(e.getCode(), e.getMsg());
    }    /**
     * 方法参数错误异常
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<Object> MethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        log.error("方法参数错误异常");
        List<String> list=new ArrayList<>();        // 从异常对象中拿到ObjectError对象
        if (!e.getBindingResult().getAllErrors().isEmpty()){
            for(ObjectError error:e.getBindingResult().getAllErrors()){
                list.add(error.getDefaultMessage().toString());
            }
        }
        // 然后提取错误提示信息进行返回
        return new Result<>(ResultCode.VALIDATE_FAILED, list);
    }
}