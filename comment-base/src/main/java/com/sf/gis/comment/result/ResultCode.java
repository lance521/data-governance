package com.sf.gis.comment.result;


/**
 * 响应枚举值
 */
public enum ResultCode {
    FAIL("400","失败"),
    UNAUTHORIZED("401","未认证"),
    INTERNAL_SERVER_ERROR("500","服务器内部错误"),
    SUCCESS("1000", "操作成功"),
    FAILED("1001", "接口错误"),
    VALIDATE_FAILED("1002", "参数校验失败"),
    ERROR("1003", "未知错误"),
    //2000系列用户错误
    USER_NOT_EXIST("2000","用户不存在"),
    USER_LOGIN_FAIL("2001","用户名或密码错误"),
    USER_NOT_LOGIN("2002","用户还未登录,请先登录"),
    NO_PERMISSION("2003","权限不足,请联系管理员");

    private final String code;
    private final String msg;

    ResultCode(String code, String msg) {
        this.code = code;
        this.msg  = msg;
    }

    public String code() {
        return code;
    }
    public String msg() {
        return msg;
    }

}
