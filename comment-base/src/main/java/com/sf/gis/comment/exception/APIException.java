/**
 * Copyright (C), 2015-2022
 * FileName: APIException
 * Author:   caiguofang
 * Date:     2022/3/12 14:09
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.exception;


import com.sf.gis.comment.result.ResultCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 异常统一处理
 * @ClassName APIException
 * @author Administrator
 * @create 2022/3/12 14:09
 * @since 1.0.0
 */
@Getter
@Setter
public class APIException extends RuntimeException{
    private String code;
    private String msg;

    public APIException() {
        this(ResultCode.ERROR);
    }

    public APIException(ResultCode failed) {
        this.code=failed.code();
        this.msg=failed.msg();
    }


    public APIException(String errorCode) {
        this.code=errorCode;
        // 获取对应的配置错误信息
        this.msg="";
    }


    public APIException(String errorCode,String msg) {
        this.code=errorCode;
        this.msg=msg;
    }
}