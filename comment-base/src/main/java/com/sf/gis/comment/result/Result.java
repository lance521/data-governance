/**
 * Copyright (C), 2015-2022
 * FileName: Result
 * Author:   caiguofang
 * Date:     2022/3/11 21:00
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.result;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sf.gis.comment.result.ResultCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 〈〉
 * @ClassName Result
 * @author Administrator
 * @create 2022/3/11 21:00
 * @since 1.0.0
 */

@Getter
@Setter
public class Result<T>   {
    private String  code;
    private String msg;
    private T data ;

    public Result(ResultCode resultCode, T data) {
        this(resultCode.code(),resultCode.msg(),data);
    }

    public Result(String resultCode, String msg) {
        this(resultCode,msg,null);
    }


    public Result(String resultCode) {
        this(resultCode,null,null);
    }

    public Result(String resultCode, String msg, T data) {
        this.code = resultCode;
        this.msg = msg;
        this.data = data;
    }

    public Result(T data) {
        this(ResultCode.SUCCESS, data);
    }

    public Result() {
        this(ResultCode.SUCCESS,null);
    }

    /**
     * 获取 json
     */
    @Override
    public   String toString(){

        JSONObject jsonObject =new JSONObject();
        jsonObject.put("code",this.code);
        jsonObject.put("msg",this.msg);
        if(data !=null){
            jsonObject.put("data",this.data);
        }
        return   jsonObject.toJSONString();

    }

}
