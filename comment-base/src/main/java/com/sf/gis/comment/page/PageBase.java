/**
 * Copyright (C), 2015-2022
 * FileName: PageBase
 * Author:   caiguofang
 * Date:     2022/3/12 12:43
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.page;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 〈〉
 * @ClassName PageBase
 * @author Administrator
 * @create 2022/3/12 12:43
 * @since 1.0.0
 */
public class PageBase<T> {
    @TableField(exist = false)
    private int page =1;
    @TableField(exist = false)
    private long size = 10;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}