/**
 * Copyright (C), 2015-2022
 * FileName: MetaHandler
 * Author:   caiguofang
 * Date:     2022/3/11 14:27
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.comment;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 自动填充默认值
 * @ClassName MetaHandler
 * @author Administrator
 * @create 2022/3/11 14:27
 * @since 1.0.0
 */
@Component
@Slf4j
public class MetaHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("creator", "admin", metaObject); //为了第一次新增就设置版本值
        this.setFieldValByName("updator", "admin", metaObject); //新增数据就默认设置0
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("creator", "admin", metaObject); //为了第一次新增就设置版本值
        this.setFieldValByName("updator", "admin", metaObject); //新增数据就默认设置0
    }
}