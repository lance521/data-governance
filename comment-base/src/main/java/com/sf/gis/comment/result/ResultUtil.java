/**
 * Copyright (C), 2015-2022
 * FileName: ResultUtil
 * Author:   caiguofang
 * Date:     2022/3/11 18:24
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.result;

/**
 * 〈〉
 * @ClassName ResultUtil
 * @author Administrator
 * @create 2022/3/11 18:24
 * @since 1.0.0
 */
public class ResultUtil<T> {

    public static Result okSuccess() {
        return new Result();
    }

    public static Result okSuccess(String message){
        return new Result(ResultCode.SUCCESS.code(),message);
    }

    public static Result okSuccess(Object data) {
        return new Result(ResultCode.SUCCESS,data);
    }

    public static Result noFailed(String code, String msg) {
        return new Result(code,msg);
    }

    public static Result noFailed(String code, String msg, Object data) {
        return new Result(code,msg,data);
    }
//
//    public static Result noFail(CommonErrorCode resultEnum) {
//        Result result = new Result();
//        result.setCode(resultEnum.getCode());
//        result.setMsg(resultEnum.getMessage());
//        return result;
//    }


}