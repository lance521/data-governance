/**
 * Copyright (C), 2015-2022
 * FileName: BaseCurdService
 * Author:   caiguofang
 * Date:     2022/3/13 14:52
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.comment.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sf.gis.comment.page.PageBase;

import java.util.List;

/**
 * 〈〉
 * @ClassName BaseCurdService
 * @author Administrator
 * @create 2022/3/13 14:52
 * @since 1.0.0
 */
public interface BaseCurdService<T extends PageBase<T>>  extends IService<T> {
    T queryByid(String id);

    List<T> queryList(T t);

    /**
     * 分页查询
     */
    IPage<T> queryPage(T t);

}