/**
 * Copyright (C), 2015-2022
 * FileName: BaseMybatiesConfig
 * Author:   caiguofang
 * Date:     2022/3/11 13:49
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.governance.sysusemanage.configuration;

/**
 * 〈〉
 * @ClassName BaseMybatiesConfig
 * @author Administrator
 * @create 2022/3/11 13:49
 * @since 1.0.0
 */

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.sf.gis.comment.comment.BaseMybatiesConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义一个配置类，mapper 扫描也可在此写上
 */
@Configuration
public class MybatiesConfig  extends BaseMybatiesConfig{

}