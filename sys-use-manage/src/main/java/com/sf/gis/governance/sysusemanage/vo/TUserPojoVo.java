package com.sf.gis.governance.sysusemanage.vo;

import com.sf.gis.governance.sysusemanage.pojo.TDepartmentPojo;
import com.sf.gis.governance.sysusemanage.pojo.TRolePojo;
import com.sf.gis.governance.sysusemanage.pojo.TUserPostPojo;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 用户管理
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@Data
public class TUserPojoVo {

    /**
     * id
     */
    private Integer id;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户岗位
     */
    private String userPost;

    /**
     * 用户岗位
     */

    private TUserPostPojo  tUserPostPojo;

    /**
     * 用户角色
     */
    private String userRole;
    /**
     * 用户角色
     */
    private TRolePojo tRolePojo;

    /**
     * 用户部门
     */
    private String userDepartment;
    /**
     * 用户部门
     */
    private TDepartmentPojo tDepartmentPojo;

    /**
     * 是否删除(0是正常 1 是删除)
     */
    private Integer isDeleted;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updater;

    /**
     * 更新时间
     */
    private Date updateTime;


}
