package com.sf.gis.governance.sysusemanage.controller.curd;


import com.sf.gis.comment.controller.CurdController;
import com.sf.gis.governance.sysusemanage.pojo.TDepartmentPojo;
import com.sf.gis.governance.sysusemanage.service.impl.crud.TDepartmentCurdServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 部门管理 前端控制器
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@RestController
@RequestMapping("/department")
@Api(value = "TDepartmentController", tags = "部门管理")
public class TDepartmentController  extends CurdController<TDepartmentCurdServiceImpl, TDepartmentPojo> {

}

