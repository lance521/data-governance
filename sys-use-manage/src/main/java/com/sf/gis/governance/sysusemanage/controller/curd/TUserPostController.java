package com.sf.gis.governance.sysusemanage.controller.curd;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sf.gis.comment.controller.CurdController;
import com.sf.gis.comment.result.Result;
import com.sf.gis.comment.result.ResultUtil;
import com.sf.gis.governance.sysusemanage.pojo.TUserPojo;
import com.sf.gis.governance.sysusemanage.pojo.TUserPostPojo;
import com.sf.gis.governance.sysusemanage.service.impl.crud.TUserCurdServiceImpl;
import com.sf.gis.governance.sysusemanage.service.impl.crud.TUserPostCurdServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 岗位信息管理 前端控制器
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@RestController
@RequestMapping("/userpost")
@Api(value = "TUserPostController", tags = "岗位信息管理")
public class TUserPostController extends CurdController<TUserPostCurdServiceImpl, TUserPostPojo > {
}

