/**
 * Copyright (C), 2015-2022
 * FileName: ResponseControllerAdviceConfig
 * Author:   caiguofang
 * Date:     2022/3/13 14:39
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.governance.sysusemanage.configuration;

import com.sf.gis.comment.aspect.ResponseControllerAdvice;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 〈〉
 * @ClassName ResponseControllerAdviceConfig
 * @author Administrator
 * @create 2022/3/13 14:39
 * @since 1.0.0
 */
@RestControllerAdvice(basePackages = {"com.sf.gis.governance.sysusemanage.controller"}) // 注意哦，这里要加上需要扫描的包
@Component
public class ResponseControllerAdviceConfig extends ResponseControllerAdvice {

}