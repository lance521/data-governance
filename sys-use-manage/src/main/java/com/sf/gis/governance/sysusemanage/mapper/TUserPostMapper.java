package com.sf.gis.governance.sysusemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.governance.sysusemanage.pojo.TUserPostPojo;

/**
 * <p>
 * 岗位信息管理 Mapper 接口
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */

public interface TUserPostMapper extends BaseMapper<TUserPostPojo> {

}
