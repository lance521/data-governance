package com.sf.gis.governance.sysusemanage.controller.curd;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sf.gis.comment.controller.CurdController;
import com.sf.gis.governance.sysusemanage.pojo.TUserPojo;
import com.sf.gis.governance.sysusemanage.service.impl.crud.TUserCurdServiceImpl;
import com.sf.gis.governance.sysusemanage.vo.TUserPojoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户管理 前端控制器
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@RestController
@RequestMapping("/user")
@Api(value = "TUserController", tags = "用户管理")
public class TUserController extends CurdController<TUserCurdServiceImpl,TUserPojo> {


    /**
     * 分页查询
     * @param tUserPojo
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "数据更全面,关联部门和角色,岗位等信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType ="query" , name  ="tUserPojo" , value = "用户信息", dataType = "TUserPojo")
    })
    @RequestMapping(path = "queryListByPage", method = RequestMethod.POST)
    public IPage<TUserPojoVo> queryListByPage(@RequestBody TUserPojo tUserPojo){
        return baseService.queryListByPage(tUserPojo);
    }
}

