/**
 * Copyright (C), 2015-2022
 * FileName: configuration
 * Author:   caiguofang
 * Date:     2022/3/12 20:01
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.governance.sysusemanage.comment;

/**
 * 〈〉
 * @ClassName configuration
 * @author Administrator
 * @create 2022/3/12 20:01
 * @since 1.0.0
 */
public class configuration {
    //删除标识
    public  final  static int deleted =1;
    public  final  static int no_deleted =0;

}