package com.sf.gis.governance.sysusemanage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication
@MapperScan("com.sf.gis.governance.sysusemanage.mapper")
@EnableOpenApi
public class SysUseManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SysUseManageApplication.class, args);
    }

}
