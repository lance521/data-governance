package com.sf.gis.governance.sysusemanage.service.impl.crud;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sf.gis.comment.service.impl.CurdServiceImpl;
import com.sf.gis.governance.sysusemanage.mapper.TUserMapper;
import com.sf.gis.governance.sysusemanage.pojo.TUserPojo;
import com.sf.gis.governance.sysusemanage.vo.TUserPojoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户管理 服务实现类
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@Service
public class TUserCurdServiceImpl extends CurdServiceImpl<TUserMapper,TUserPojo> {
    @Autowired
    TUserMapper tUserMapper ;

    public IPage<TUserPojoVo> queryListByPage(TUserPojo tUserPojo) {
        IPage<TUserPojoVo> page = new Page<>(tUserPojo.getPage(),tUserPojo.getSize());
        return   tUserMapper.queryTUserPojoByPage(page, tUserPojo);
    }

}
