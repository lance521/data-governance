package com.sf.gis.governance.sysusemanage.service.impl.crud;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sf.gis.comment.service.impl.CurdServiceImpl;
import com.sf.gis.governance.sysusemanage.mapper.TDepartmentMapper;
import com.sf.gis.governance.sysusemanage.pojo.TDepartmentPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门管理 服务实现类
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@Service
public class TDepartmentCurdServiceImpl
        extends CurdServiceImpl<TDepartmentMapper, TDepartmentPojo>{
}
