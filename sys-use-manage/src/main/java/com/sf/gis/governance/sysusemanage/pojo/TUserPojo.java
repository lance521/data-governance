package com.sf.gis.governance.sysusemanage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sf.gis.comment.page.PageBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户管理
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@Data
@TableName("t_user")
public class TUserPojo extends PageBase<TUserPojo> {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户岗位
     */
    private String userPost;

    /**
     * 用户岗位
     */
    @TableField(exist = false)
    private String userPostName;

    /**
     * 用户角色
     */
    private String userRole;
    /**
     * 用户角色
     */
    @TableField(exist = false)
    private String userRoleName;

    /**
     * 用户部门
     */
    private String userDepartment;
    /**
     * 用户部门
     */
    @TableField(exist = false)
    private String userDepartmentName;

    /**
     * 是否删除(0是正常 1 是删除)
     */
    private Integer isDeleted;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updater;

    /**
     * 更新时间
     */
    private Date updateTime;


}
