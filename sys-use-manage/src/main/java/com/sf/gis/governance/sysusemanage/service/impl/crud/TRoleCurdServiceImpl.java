package com.sf.gis.governance.sysusemanage.service.impl.crud;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sf.gis.comment.service.impl.CurdServiceImpl;
import com.sf.gis.governance.sysusemanage.mapper.TRoleMapper;
import com.sf.gis.governance.sysusemanage.pojo.TRolePojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色管理 服务实现类
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@Service
public class TRoleCurdServiceImpl
        extends CurdServiceImpl<TRoleMapper, TRolePojo>{
}
