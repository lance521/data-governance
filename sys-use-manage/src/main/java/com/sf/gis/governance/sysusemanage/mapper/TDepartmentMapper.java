package com.sf.gis.governance.sysusemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.governance.sysusemanage.pojo.TDepartmentPojo;

/**
 * <p>
 * 部门管理 Mapper 接口
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
public interface TDepartmentMapper extends BaseMapper<TDepartmentPojo> {

}
