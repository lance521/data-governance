package com.sf.gis.governance.sysusemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.governance.sysusemanage.pojo.TRolePojo;

/**
 * <p>
 * 角色管理 Mapper 接口
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
public interface TRoleMapper extends BaseMapper<TRolePojo> {

}
