package com.sf.gis.governance.sysusemanage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sf.gis.comment.page.PageBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 岗位信息管理
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */
@TableName("t_user_post")
@Data
public class TUserPostPojo extends PageBase<TUserPostPojo> {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 岗位编码
     */
    private String postCode;

    /**
     * 岗位级别
     */
    private String postLevel;

    /**
     * 岗位名称
     */
    private String postName;

    /**
     * 是否删除(0是正常 1 是删除)
     */
    private Integer isDeleted;

    /**
     * 备注
     */
    private String  remark;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String updater;

    /**
     * 更新时间
     */
    private Date updateTime;


}
