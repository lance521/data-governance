/**
 * Copyright (C), 2015-2022
 * FileName: BaseSwaggerConfig
 * Author:   caiguofang
 * Date:     2022/3/13 18:23
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.governance.sysusemanage.configuration;

import com.sf.gis.comment.comment.BaseSwaggerConfig;
import org.springframework.context.annotation.Configuration;

/**
 * 〈〉
 * @ClassName BaseSwaggerConfig
 * @author Administrator
 * @create 2022/3/13 18:23
 * @since 1.0.0
 */
@Configuration
public class SwaggerConfig extends BaseSwaggerConfig {

}