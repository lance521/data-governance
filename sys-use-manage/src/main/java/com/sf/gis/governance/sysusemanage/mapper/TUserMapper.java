package com.sf.gis.governance.sysusemanage.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.sf.gis.governance.sysusemanage.pojo.TUserPojo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.governance.sysusemanage.vo.TUserPojoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 用户管理 Mapper 接口
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-12
 */

public interface TUserMapper extends BaseMapper<TUserPojo> {

    IPage<TUserPojoVo> queryTUserPojoByPage(IPage<TUserPojoVo> page, @Param("pojo") TUserPojo pojo);
}
