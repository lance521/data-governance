DROP TABLE IF EXISTS Sales.t_user_post;
CREATE TABLE IF NOT EXISTS Sales.t_user_post
(
    id   INT    AUTO_INCREMENT  comment 'id',
    post_code   VARCHAR(50)   not null  comment '岗位编码',
    post_level   TINYINT   not null  comment '岗位级别',
    post_name   VARCHAR(100)   not null  comment '岗位名称',
    is_deleted   TINYINT   not null  comment '是否删除(0是正常 1 是删除)',
    remark   VARCHAR(100)     comment '备注',
    creator   VARCHAR(50)    not null default 'admin' comment '创建人',
    create_time   DATETIME     not null default CURRENT_TIMESTAMP comment '创建时间',
    updater   VARCHAR(50)    not null default 'admin' comment '更新人',
    update_time   DATETIME     not null default CURRENT_TIMESTAMP comment '更新时间',
    PRIMARY KEY(id)
) engine=innodb  default charset=utf8;
alter table  Sales.t_user_post comment '岗位信息管理';
DROP TABLE IF EXISTS Sales.t_department;
CREATE TABLE IF NOT EXISTS Sales.t_department
(
    id   INT    AUTO_INCREMENT  comment 'id',
    department_code   VARCHAR(50)   not null  comment '部门编码',
    department_level   TINYINT   not null  comment '部门级别',
    department_name   VARCHAR(50)   not null  comment '部门名称',
    is_deleted   TINYINT   not null  comment '是否删除(0是正常 1 是删除)',
    remark   VARCHAR(100)     comment '备注',
    creator   VARCHAR(50)    not null default 'admin' comment '创建人',
    create_time   DATETIME     not null default CURRENT_TIMESTAMP comment '创建时间',
    updater   VARCHAR(50)    not null default 'admin' comment '更新人',
    update_time   DATETIME     not null default CURRENT_TIMESTAMP comment '更新时间',
    PRIMARY KEY(id)
) engine=innodb  default charset=utf8;
alter table  Sales.t_department comment '部门管理';
DROP TABLE IF EXISTS Sales.t_Role;
CREATE TABLE IF NOT EXISTS Sales.t_Role
(
    id   INT    AUTO_INCREMENT  comment 'id',
    role_code   VARCHAR(50)   not null  comment '角色编码',
    role_name   VARCHAR(50)   not null  comment '角色名称',
    is_deleted   TINYINT   not null  comment '是否删除(0是正常 1 是删除)',
    remark   VARCHAR(100)     comment '备注',
    creator   VARCHAR(50)    not null default 'admin' comment '创建人',
    create_time   DATETIME     not null default CURRENT_TIMESTAMP comment '创建时间',
    updater   VARCHAR(50)    not null default 'admin' comment '更新人',
    update_time   DATETIME     not null default CURRENT_TIMESTAMP comment '更新时间',
    PRIMARY KEY(id)
) engine=innodb  default charset=utf8;
alter table  Sales.t_Role comment '角色管理';
DROP TABLE IF EXISTS Sales.t_user;
CREATE TABLE IF NOT EXISTS Sales.t_user
(
    id   INT    AUTO_INCREMENT  comment 'id',
    user_code   VARCHAR(50)   not null  comment '用户编码',
    user_name   VARCHAR(50)   not null  comment '用户名称',
    user_post   VARCHAR(50)   not null  comment '用户岗位',
    user_role   VARCHAR(50)   not null  comment '用户角色',
    user_department   VARCHAR(50)   not null  comment '用户部门',
    is_deleted   TINYINT     comment '是否删除(0是正常 1 是删除)',
    remark   VARCHAR(100)     comment '备注',
    creator   VARCHAR(50)    not null default 'admin' comment '创建人',
    create_time   DATETIME     not null default CURRENT_TIMESTAMP comment '创建时间',
    updater   VARCHAR(50)    not null default 'admin' comment '更新人',
    update_time   DATETIME     not null default CURRENT_TIMESTAMP comment '更新时间',
    PRIMARY KEY(id)
) engine=innodb  default charset=utf8;
alter table  Sales.t_user comment '用户管理';
