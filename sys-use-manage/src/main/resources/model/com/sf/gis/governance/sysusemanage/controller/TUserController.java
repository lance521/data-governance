package com.sf.gis.governance.sysusemanage.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户管理 前端控制器
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-13
 */
@RestController
@RequestMapping("/t-user")
public class TUserController {

}

