package com.sf.gis.governance.sysusemanage.mappers;

import com.sf.gis.governance.sysusemanage.pojo.TUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户管理 Mapper 接口
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-13
 */
public interface TUserMapper extends BaseMapper<TUser> {

}
