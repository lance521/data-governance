package com.sf.gis.governance.sysusemanage.service;

import com.sf.gis.governance.sysusemanage.pojo.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户管理 服务类
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-13
 */
public interface TUserService extends IService<TUser> {

}
