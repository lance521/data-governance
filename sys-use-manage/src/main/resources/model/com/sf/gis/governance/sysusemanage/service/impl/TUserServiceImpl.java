package com.sf.gis.governance.sysusemanage.service.impl;

import com.sf.gis.governance.sysusemanage.pojo.TUser;
import com.sf.gis.governance.sysusemanage.mappers.TUserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户管理 服务实现类
 * </p>
 *
 * @author caiguofang
 * @since 2022-03-13
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements TUserService {

}
