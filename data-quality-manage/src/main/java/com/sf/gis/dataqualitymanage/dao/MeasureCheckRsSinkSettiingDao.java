/**
 * Copyright (C), 2015-2022
 * FileName: MeasureCheckRsSinkSettiingDao
 * Author:   caiguofang
 * Date:     2022/3/10 19:46
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sf.gis.dataqualitymanage.pojo.MeasureCheckRsSinkSettiingPojo;

/**
 * 数据质量管理
 * @ClassName MeasureCheckRsSinkSettiingDao
 * @author Administrator
 * @create 2022/3/10 19:46
 * @since 1.0.0
 */
public interface MeasureCheckRsSinkSettiingDao
        extends BaseMapper<MeasureCheckRsSinkSettiingPojo>{

}