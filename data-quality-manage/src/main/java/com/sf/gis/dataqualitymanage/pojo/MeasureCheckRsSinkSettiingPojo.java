/**
 * Copyright (C), 2015-2022
 * FileName: MeasureCheckRsSinkSettiingPojo
 * Author:   caiguofang
 * Date:     2022/3/10 19:19
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sf.gis.dataqualitymanage.vo.BaseEntity;
import lombok.Data;

/**
 * 〈〉
 * @ClassName MeasureCheckRsSinkSettiingPojo
 * @author Administrator
 * @create 2022/3/10 19:19
 * @since 1.0.0
 */
@Data
@TableName(value = "measure_check_rs_sink_settiing")
public class MeasureCheckRsSinkSettiingPojo extends BasePojo {
    private String id;
    private String checkRsSinkCode;
    private String checkRsSinkType;
    private String checkRsSinkName;
    private String remark;
    private String deleted;
    private String version;
    private String checkMeasureId;
    private String checkRuleId;
    private String qualityCheckId;
    private String sinkConCode;
    private String srcDbCode;
}