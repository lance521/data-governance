/**
 * Copyright (C), 2015-2022
 * FileName: BaseMeasureEntity
 * Author:   caiguofang
 * Date:     2022/3/10 19:11
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.sf.gis.comment.page.PageBase;
import lombok.Data;

/**
 * 〈〉
 * @ClassName BaseMeasureEntity
 * @author Administrator
 * @create 2022/3/10 19:11
 * @since 1.0.0
 */

@Data
public class BaseEntity extends PageBase {
    private String creator;
    private String createTime;
    private String updator;
    private String updateTime;
}