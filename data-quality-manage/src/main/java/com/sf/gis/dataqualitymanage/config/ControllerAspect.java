/**
 * Copyright (C), 2015-2022
 * FileName: ControllerAspect
 * Author:   caiguofang
 * Date:     2022/3/12 14:24
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.config;

import com.sf.gis.comment.aspect.ResponseControllerAdvice;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 〈〉
 * @ClassName ControllerAspect
 * @author Administrator
 * @create 2022/3/12 14:24
 * @since 1.0.0
 */
@RestControllerAdvice(basePackages = {"com.sf.gis.dataqualitymanage.controllers"}) // 注意哦，这里要加上需要扫描的包
@Component
public class ControllerAspect  extends ResponseControllerAdvice {
}