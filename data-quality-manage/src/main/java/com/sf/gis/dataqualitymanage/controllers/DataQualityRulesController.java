/**
 * Copyright (C), 2015-2022
 * FileName: DataQualityRulesController
 * Author:   caiguofang
 * Date:     2022/3/8 11:31
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.controllers;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sf.gis.comment.exception.APIException;
import com.sf.gis.comment.result.Result;
import com.sf.gis.comment.result.ResultUtil;
import com.sf.gis.dataqualitymanage.dao.MeasureCheckRsSinkSettiingDao;
import com.sf.gis.dataqualitymanage.vo.MeasureCheckRsSinkSettiingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 数据质量规则管理, 增删改查
 * @ClassName DataQualityRulesController
 * @author Administrator
 * @create 2022/3/8 11:31
 * @since 1.0.0
 */

@RestController
@RequestMapping("dataqualityrules")
public class DataQualityRulesController {

    @Autowired
    MeasureCheckRsSinkSettiingDao measureCheckRsSinkSettiingdao;

    @RequestMapping("test")
    public Result<MeasureCheckRsSinkSettiingVo>
    pageList(@RequestBody MeasureCheckRsSinkSettiingVo entity){
        return ResultUtil.okSuccess(measureCheckRsSinkSettiingdao.selectPage(new Page(2,10), null ));
    }

    @RequestMapping("test2")
    public String
    pageList() throws Exception {
        throw new APIException("20003");
    }

}