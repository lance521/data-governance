package com.sf.gis.dataqualitymanage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.sf.gis.dataqualitymanage.dao")
public class DataQualityManageApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataQualityManageApplication.class, args);
    }

}
