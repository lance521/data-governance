/**
 * Copyright (C), 2015-2022
 * FileName: GlobalExceptionHandler
 * Author:   caiguofang
 * Date:     2022/3/12 15:50
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.config;

import com.sf.gis.comment.exception.GlobalExceptionHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 〈〉
 * @ClassName GlobalExceptionHandler
 * @author Administrator
 * @create 2022/3/12 15:50
 * @since 1.0.0
 */
@RestControllerAdvice
@Component
public class ExceptionHandler extends GlobalExceptionHandler {

}