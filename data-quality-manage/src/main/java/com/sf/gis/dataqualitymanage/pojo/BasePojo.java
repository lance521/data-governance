/**
 * Copyright (C), 2015-2022
 * FileName: BaseMeasureEntity
 * Author:   caiguofang
 * Date:     2022/3/10 19:11
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 〈〉
 * @ClassName BaseMeasureEntity
 * @author Administrator
 * @create 2022/3/10 19:11
 * @since 1.0.0
 */

@Data
public class BasePojo {
    @TableField(value = "creator", fill = FieldFill.INSERT)
    private String creator;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private String createTime;
    @TableField(value = "updator", fill = FieldFill.INSERT_UPDATE)
    private String updator;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private String updateTime;
}