/**
 * Copyright (C), 2015-2022
 * FileName: PageTest
 * Author:   caiguofang
 * Date:     2022/3/11 17:11
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.sf.gis.dataqualitymanage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sf.gis.dataqualitymanage.dao.MeasureCheckRsSinkSettiingDao;
import com.sf.gis.dataqualitymanage.pojo.MeasureCheckRsSinkSettiingPojo;
import com.sf.gis.dataqualitymanage.vo.MeasureCheckRsSinkSettiingVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 〈〉
 * @ClassName PageTest
 * @author Administrator
 * @create 2022/3/11 17:11
 * @since 1.0.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class PageTest {

    @Autowired
    MeasureCheckRsSinkSettiingDao measureCheckRsSinkSettiingDao;
    /**
     * 分页测试
     */
    @Test
    public void selectPage(){
        MeasureCheckRsSinkSettiingVo measureCheckRsSinkSettiingPojo = new MeasureCheckRsSinkSettiingVo();
        QueryWrapper<MeasureCheckRsSinkSettiingPojo> queryWrapper=new QueryWrapper<>();
        queryWrapper.gt("id",18);

        Page<MeasureCheckRsSinkSettiingPojo> page= null;// (Page<MeasureCheckRsSinkSettiingPojo>) measureCheckRsSinkSettiingPojo.getPageSet();

        IPage<MeasureCheckRsSinkSettiingPojo> userPage = measureCheckRsSinkSettiingDao.selectPage(page, queryWrapper);
        System.out.println("总页数："+userPage.getPages());
        System.out.println("总记录数："+userPage.getTotal());
        List<MeasureCheckRsSinkSettiingPojo> records = userPage.getRecords();
        records.forEach(System.out::println);

    }

}